import {Component, OnInit} from '@angular/core';
import {AuthService} from "../service/auth.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title: string;

  constructor(private authService: AuthService) {
    this.title = 'Spring Boot - Angular Application';
  }

  ngOnInit() {
  }

  logout() {
    this.authService.doLogout();
  }
}
