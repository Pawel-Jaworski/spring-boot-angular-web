package pl.net.innotec.springbootangularweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.net.innotec.springbootangularweb.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    public UserEntity getUserEntityByEmail(String email);
}
