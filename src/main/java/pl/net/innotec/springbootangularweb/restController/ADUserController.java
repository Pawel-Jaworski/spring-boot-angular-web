package pl.net.innotec.springbootangularweb.restController;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.net.innotec.springbootangularweb.service.ADUserService;

@RestController
@RequiredArgsConstructor
public class ADUserController {

    private final ADUserService adUserService;

    @GetMapping("/email")
    public String getEmail(Authentication authentication) {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @GetMapping("/object")
    public Object getObject(Authentication authentication) {
        return adUserService.getObjectByUsername("testowy", "zaq1@WSX");
    }

}
