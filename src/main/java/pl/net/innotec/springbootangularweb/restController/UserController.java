package pl.net.innotec.springbootangularweb.restController;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.net.innotec.springbootangularweb.dto.UserDTO;
import pl.net.innotec.springbootangularweb.entity.UserEntity;
import pl.net.innotec.springbootangularweb.service.UserService;

import java.util.List;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("/api")
public class UserController {

    private final UserService userService;

    @GetMapping("/users")
    public List<UserDTO> getUsers() {
        return userService.getUsers();
    }

    @PostMapping("/users")
    public void addUser(@RequestBody UserEntity userEntity) {
        userService.addUser(userEntity);
    }
}
