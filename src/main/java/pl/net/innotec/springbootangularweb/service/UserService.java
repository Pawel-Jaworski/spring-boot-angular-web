package pl.net.innotec.springbootangularweb.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.net.innotec.springbootangularweb.dto.UserDTO;
import pl.net.innotec.springbootangularweb.entity.UserEntity;
import pl.net.innotec.springbootangularweb.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public List<UserDTO> getUsers() {
        return userRepository.findAll().stream().map(UserDTO::new).collect(Collectors.toList());
    }

    public UserDTO getUserByEmail(String email) {
        return new UserDTO(userRepository.getUserEntityByEmail(email));
    }

    public void addUser(UserEntity userEntity) {
        userRepository.save(userEntity);
    }
}
