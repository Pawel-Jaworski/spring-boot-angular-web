package pl.net.innotec.springbootangularweb.service;

import com.imperva.ddc.core.Connector;
import com.imperva.ddc.core.language.PhraseOperator;
import com.imperva.ddc.core.language.QueryAssembler;
import com.imperva.ddc.core.language.Sentence;
import com.imperva.ddc.core.query.*;
import com.imperva.ddc.service.DirectoryConnectorService;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static pl.net.innotec.springbootangularweb.config.LdapConstatnts.*;

@Configuration
@PropertySource("classpath:ldap.properties")
@Service
public class ADUserService {

    public String getEmailByUsername(String username, String password) {

        Endpoint endpoint = new Endpoint();
        endpoint.setSecuredConnection(false);
        endpoint.setSecuredConnectionSecondary(false);
        endpoint.setPort(LDAP_PORT);
        endpoint.setHost(LDAP_HOST);
        endpoint.setPassword(password);
        endpoint.setUserAccountName(username + "@" + LDAP_DOMAIN);//* You can use the user's Distinguished Name as well

        ConnectionResponse connectionResponse = DirectoryConnectorService.authenticate(endpoint);
        boolean succeeded = !connectionResponse.isError();

        QueryRequest queryRequest = new QueryRequest();
        queryRequest.setDirectoryType(DirectoryType.MS_ACTIVE_DIRECTORY);
        queryRequest.setEndpoints(new ArrayList<Endpoint>() {{
            add(endpoint);
        }});
        queryRequest.setSizeLimit(1000);
        queryRequest.setTimeLimit(1000);
        queryRequest.setObjectType(ObjectType.USER);
        queryRequest.addRequestedField(FieldType.EMAIL);

        QueryAssembler queryAssembler;
        queryAssembler = new QueryAssembler();
        Sentence firstNameSentence = queryAssembler.addPhrase(FieldType.LOGON_NAME, PhraseOperator.EQUAL, username).closeSentence();

        queryRequest.addSearchSentence(firstNameSentence);

        QueryResponse queryResponse;
        try (Connector connector = new Connector(queryRequest)) {
            queryResponse = connector.execute();
        }

        String email = (String) queryResponse.getAll().stream().flatMap(res -> res.getValue().stream()).findFirst().get().getValue();

        return email;

    }

    public Object getObjectByUsername(String username, String password) {

        Endpoint endpoint = new Endpoint();
        endpoint.setSecuredConnection(false);
        endpoint.setSecuredConnectionSecondary(false);
        endpoint.setPort(LDAP_PORT);
        endpoint.setHost(LDAP_HOST);
        endpoint.setPassword(password);
        endpoint.setUserAccountName(username + "@" + LDAP_DOMAIN);//* You can use the user's Distinguished Name as well

        ConnectionResponse connectionResponse = DirectoryConnectorService.authenticate(endpoint);
        boolean succeeded = !connectionResponse.isError();

        QueryRequest queryRequest = new QueryRequest();
        queryRequest.setDirectoryType(DirectoryType.MS_ACTIVE_DIRECTORY);
        queryRequest.setEndpoints(new ArrayList<Endpoint>() {{
            add(endpoint);
        }});
        queryRequest.setSizeLimit(1000);
        queryRequest.setTimeLimit(1000);
        queryRequest.setObjectType(ObjectType.USER);
        queryRequest.addRequestedField(FieldType.USER_PRINCIPAL_NAME);
        queryRequest.addRequestedField(FieldType.LOGON_NAME);
        queryRequest.addRequestedField(FieldType.DISTINGUISHED_NAME);

        QueryAssembler queryAssembler;
        queryAssembler = new QueryAssembler();
        Sentence firstNameSentence = queryAssembler.addPhrase(FieldType.LOGON_NAME, PhraseOperator.EQUAL, username).closeSentence();

        queryRequest.addSearchSentence(firstNameSentence);

        QueryResponse queryResponse;
        try (Connector connector = new Connector(queryRequest)) {
            queryResponse = connector.execute();
        }

        Object object = queryResponse.getAll();

        return object;

    }
}
