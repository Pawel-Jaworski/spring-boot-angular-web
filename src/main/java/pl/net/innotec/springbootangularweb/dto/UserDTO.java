package pl.net.innotec.springbootangularweb.dto;

import lombok.*;
import pl.net.innotec.springbootangularweb.entity.ServiceEntity;
import pl.net.innotec.springbootangularweb.entity.UserEntity;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class UserDTO {

    private long id;
    private String name;
    private String email;
    private List<ServiceEntity> services;

    public UserDTO(UserEntity userEntity) {
        this.id = userEntity.getId();
        this.name = userEntity.getName();
        this.email = userEntity.getEmail();
    }
}
