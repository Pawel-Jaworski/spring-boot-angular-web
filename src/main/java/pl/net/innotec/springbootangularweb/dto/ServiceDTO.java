package pl.net.innotec.springbootangularweb.dto;

import lombok.*;
import pl.net.innotec.springbootangularweb.entity.ServiceEntity;
import pl.net.innotec.springbootangularweb.entity.UserEntity;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ServiceDTO {

    private long id;
    private String name;
    private LocalDateTime expirationDate;
    private String details;
    private UserEntity owner;

    public ServiceDTO(ServiceEntity serviceEntity) {
        this.id = serviceEntity.getId();
        this.name = serviceEntity.getName();
        this.expirationDate = serviceEntity.getExpirationDate();
        this.details = serviceEntity.getDetails();
        this.owner = serviceEntity.getOwner();
    }
}
