package pl.net.innotec.springbootangularweb;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.net.innotec.springbootangularweb.dto.UserDTO;
import pl.net.innotec.springbootangularweb.entity.ServiceEntity;
import pl.net.innotec.springbootangularweb.entity.UserEntity;
import pl.net.innotec.springbootangularweb.service.UserService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.stream.Stream;

@SpringBootApplication
public class SpringBootAngularWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAngularWebApplication.class, args);
    }

    @Bean
    CommandLineRunner init(UserService userService) {
        return args -> {
            Stream.of("John", "Julie", "Jennifer", "Helen", "Rachel").forEach(name -> {
                UserEntity userEntity = UserEntity.builder()
                        .name(name)
                        .email(name.toLowerCase() + "@domain.com")
                        .services(new ArrayList<ServiceEntity>())
                        .build();
                userService.addUser(userEntity);
            });
            userService.getUsers().forEach(System.out::println);
            UserDTO john = userService.getUserByEmail("john@domain.com");

            UserEntity johnE = UserEntity.builder()
                    .name(john.getName())
                    .email(john.getEmail())
                    .services(john.getServices())
                    .build();

            ServiceEntity usluga1 = ServiceEntity.builder()
                    .name("usluga1")
                    .expirationDate(LocalDateTime.now())
                    .details("some details in json")
                    .owner(johnE)
                    .build();

            System.out.println(usluga1);
        };
    }


}
