package pl.net.innotec.springbootangularweb.config;

public class LdapConstatnts {
    public static final String LDAP_DOMAIN = "test.local";
    public static final String LDAP_HOST = "192.168.56.101";
    public static final int LDAP_PORT = 389;
}
